﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cod4WeaponfilesEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void FilePathTextChange(object sender, TextChangedEventArgs e)
        {
            if (Directory.Exists(FilePath.Text))
            {
                var files = Directory.EnumerateFiles(FilePath.Text).ToList();
                // path + \
                var filesShort = files.Select(f => f.Substring(FilePath.Text.Length + 1)).ToList();
                ComboBoxLeft.ItemsSource = filesShort;
                ComboBoxRight.ItemsSource = filesShort;
            }
        }

        private string GetPath(string end)
        {
            return $"{FilePath.Text}\\{end}";
        }

        private void AddWeaponToDict(System.Windows.Controls.ComboBox comboBox, int index, Dictionary<string, List<string>> weapons)
        {
            if (comboBox.SelectedItem is not null)
            {
                var path = GetPath(comboBox.SelectedItem.ToString());
                if (File.Exists(path))
                {
                    var values = File.ReadAllLines(path, Encoding.UTF8)[0].Split('\\');
                    // i has to be 1 because first value is WEAPONFILE
                    for (int i = 1; i < values.Length; i += 2)
                    {
                        if (!weapons.TryGetValue(values[i], out List<string>? value))
                        {
                            value = new List<string>(index + 1);
                            weapons[values[i]] = value;
                        }
                        if (value.Count < index + 1)
                        {
                            for (int j = value.Count; j < index + 1; j++)
                            {
                                value.Add("");
                            }
                        }
                        value[index] = values[i + 1];
                    }
                }
            }
        }

        private Dictionary<string, List<string>> GetWeaponsAsDict()
        {
            Dictionary<string, List<string>> weapons = new();
            // left is 0 indexed
            AddWeaponToDict(ComboBoxLeft, 0, weapons);
            // right is 1 indexed
            AddWeaponToDict(ComboBoxRight, 1, weapons);
            return weapons;
        }

        private DataTable GenDataTable(Dictionary<string, List<string>> weapons, List<string>? addCol = null)
        {
            addCol ??= new List<string>();
            var dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns["Name"].ReadOnly = true;
            if (ComboBoxLeft.SelectedItem is not null)
                dt.Columns.Add(ComboBoxLeft.SelectedItem.ToString());
            if (ComboBoxRight.SelectedItem is not null)
                dt.Columns.Add(ComboBoxRight.SelectedItem.ToString());
            foreach (var col in addCol)
            {
                dt.Columns.Add(col);
            }
            foreach (var (k, v) in weapons)
            {
                var dr = dt.NewRow();
                dr["Name"] = k;
                for (int i = 0; i < v.Count; i++)
                {
                    dr[i + 1] = v[i];
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        private void ComboBoxLeftSelectionChange(object sender, SelectionChangedEventArgs e)
        {
            // display in a DataGrid
            DataGridLeft.ItemsSource = GenDataTable(SearchForTextInDict(GetWeaponsAsDict(), TextBoxSearch.Text)).DefaultView;
        }

        private void ComboBoxRightSelectionChange(object sender, SelectionChangedEventArgs e)
        {
            // display in a DataGrid
            DataGridLeft.ItemsSource = GenDataTable(SearchForTextInDict(GetWeaponsAsDict(), TextBoxSearch.Text)).DefaultView;
        }

        private Dictionary<string, List<string>> SearchForTextInDict(Dictionary<string, List<string>> weapons, string text)
        {
            if (text == "Search")
            {
                return weapons;
            }
            Dictionary<string, List<string>> wText = new();
            foreach (var (k, v) in weapons)
            {
                // checks if name or description contains
                if (k.ToLower().Contains(text.ToLower()))
                {
                    wText.Add(k, v);
                    continue;
                }
                if (v.Find(e => e.ToLower().Contains(text.ToLower())) is not null)
                {
                    wText.Add(k, v);
                }
            }
            return wText;
        }

        private void TextBoxSearchTextChange(object sender, TextChangedEventArgs e)
        {
            DataGridLeft.ItemsSource = GenDataTable(SearchForTextInDict(GetWeaponsAsDict(), TextBoxSearch.Text)).DefaultView;
        }

        private void DiffButtonClick(object sender, RoutedEventArgs e)
        {
            // for now we only diff 2nd and 3rd row(1st one being the names)
            var weapons = SearchForTextInDict(GetWeaponsAsDict(), TextBoxSearch.Text);
            foreach (var (_, v) in weapons)
            {
                if (int.TryParse(v[0], out int resultLeft) && int.TryParse(v[1], out int resultRight))
                    v.Add((resultLeft - resultRight).ToString());
                else
                    v.Add("");
            }
            DataGridLeft.ItemsSource = GenDataTable(weapons, new List<string>() { "diff" }).DefaultView;
            var propertyForeground = DependencyProperty.Register("Foreground", Type.GetType("string"), Type.GetType("string"));
            Setter setterForeground = new()
            {
                Property = propertyForeground,
                Value = "orange"
            };
            var propertyBackground = DependencyProperty.Register("Background", Type.GetType("string"), Type.GetType("string"));
            Setter setterBackground = new()
            {
                Property = propertyBackground,
                Value = "blue"
            };
            DataGridLeft.Columns[0].CellStyle.Setters.Add(setterForeground);
            DataGridLeft.Columns[0].CellStyle.Setters.Add(setterBackground);
            // = new DataGridViewCellStyle { ForeColor = System.Drawing.Color.Orange, BackColor = System.Drawing.Color.Blue };
            //dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Style = new DataGridViewCellStyle { ForeColor = System.Drawing.Color.Orange, BackColor = System.Drawing.Color.Blue };
            /*for (int i = 0; i < DataGridLeft.Items.Count; i++)
            {
                var rd = new ResourceDictionary { { "fgColor", Colors.Red } };
                DataGridLeft.Columns[1].GetCellContent(DataGridLeft.Items[i]).Resources = rd;
            }*/
            /*foreach (DataRowView drv in DataGridLeft.Items)
            {
                for (int i = 0; i < DataGridLeft.Columns.Count; i++)
                {
                    DataGridLeft.Columns[1].GetCellContent(drv).Resources = new ResourceDictionary { { "fgColor", Colors.Red } };
                }
            }*/
        }
    }
}
