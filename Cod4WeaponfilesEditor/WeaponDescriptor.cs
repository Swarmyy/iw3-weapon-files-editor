﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cod4WeaponfilesEditor
{
    internal class WeaponDescriptor
    {
        [ReadOnly(true)]
        public string Name { get; }
        public string Description { get; set; }
        public WeaponDescriptor(string name, string description)
        {
            Name = name; Description = description;
        }
    }
}
